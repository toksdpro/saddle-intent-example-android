# Saddle Intent Example: Android


Sample code triggering payment from a 3rd party calling app using Saddle

private static final int REQUEST_CODE = 111;

private void sendTransaction() {

    try {
        Intent intent = new Intent("com.smartpesa.netpluspay.MainActivity");
        intent.putExtra("amount", amountEt.getText().toString().trim());
        intent.putExtra("orderNo", orderNo.getText().toString().trim());
        intent.putExtra("narrative", narrative.getText().toString().trim());
        intent.putExtra("email", email.getText().toString().trim());
        intent.putExtra("merchantID", externalReferenceEt.getText().toString().trim());
        startActivityForResult(intent, REQUEST_CODE);
    } catch (ActivityNotFoundException ex) {
        ex.printStackTrace();
        //Log.e("Main", "Second application is not installed!");
        showSnackbar("Saddle App is not installed!");
    }

    
}


@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == REQUEST_CODE ) {
        if (data == null) {
            // This can happen if Saddle was uninstalled or crashed while we're waiting for a
            // result.
            showSnackbar("No result from App.");
            return;
        } else if (resultCode == RESULT_OK) {


            String resultString = "Success\n" +
                    data.getStringExtra("transactionId") + "\n" +
                    data.getStringExtra("status") + "\n" +
                    data.getStringExtra("amount") + "\n" ;

            this.result.setText(resultString);
            showSnackbar("Transaction Completed.");
        } else {
            String resultString = "Error\n" +
                    data.getStringExtra("status") + "\n" +
                    data.getStringExtra("amount") + "\n" +
                    data.getStringExtra("transactionId") + "\n" ;

            this.result.setText(resultString);
            showSnackbar("Transaction Not Completed.");

        }
    }
}


private void showSnackbar(String message) {
    final Snackbar snackbar = Snackbar.make(fab, message, Snackbar.LENGTH_LONG);
    snackbar.setAction("Dismiss", new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            snackbar.dismiss();
        }
    });
    snackbar.show();
}



