package com.smartpesa.sampleintent;

import com.smartpesa.intent.SpConnect;
import com.smartpesa.intent.TransactionArgument;
import com.smartpesa.intent.TransactionType;
import com.smartpesa.intent.result.TransactionError;
import com.smartpesa.intent.result.TransactionResult;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {


    private static final int REQUEST_CODE = 111;

    private EditText amountEt, orderNo, narrative, email, externalReferenceEt;
    private FloatingActionButton fab;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        amountEt = (EditText) findViewById(R.id.amount);
        orderNo = (EditText) findViewById(R.id.order);
        narrative = (EditText) findViewById(R.id.narrative);
        email = (EditText) findViewById(R.id.email);
        result = (TextView) findViewById(R.id.result);
        externalReferenceEt = (EditText) findViewById(R.id.external_reference);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText("");
                sendTransaction();
            }
        });
    }

    private void sendTransaction() {

        try {
            Intent intent = new Intent("com.smartpesa.netpluspay.MainActivity");
            intent.putExtra("amount", amountEt.getText().toString().trim());
            intent.putExtra("orderNo", orderNo.getText().toString().trim());
            intent.putExtra("narrative", narrative.getText().toString().trim());
            intent.putExtra("email", email.getText().toString().trim());
            intent.putExtra("merchantID", externalReferenceEt.getText().toString().trim());
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            //Log.e("Main", "Second application is not installed!");
            showSnackbar("Saddle App is not installed!");
        }


    }

    private void showSnackbar(String message) {
        final Snackbar snackbar = Snackbar.make(fab, message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE ) {
            if (data == null) {
                // This can happen if Saddle was uninstalled or crashed while we're waiting for a
                // result.
                showSnackbar("No result from App.");
                return;
            } else if (resultCode == RESULT_OK) {


                String resultString = "Success\n" +
                        data.getStringExtra("transactionId") + "\n" +
                        data.getStringExtra("status") + "\n" +
                        data.getStringExtra("amount") + "\n" ;

                this.result.setText(resultString);
                showSnackbar("Transaction Completed.");
            } else {
                String resultString = "Error\n" +
                        data.getStringExtra("status") + "\n" +
                        data.getStringExtra("amount") + "\n" +
                        data.getStringExtra("transactionId") + "\n" ;

                this.result.setText(resultString);
                showSnackbar("Transaction Not Completed.");

            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }
}
